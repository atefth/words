package atef.haque.odesk;

import java.util.Random;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class WordsActivity extends Activity implements OnClickListener {
	private Button gen;
	private Button check;
	private TextView word;
	private TextView solution;
	private EditText input;
	String words[];
	String theWord;
	Random r;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        init();
    }
    
    public void init(){
    	
		r = new Random();
    	words = this.getResources().getStringArray(R.array.words);
    	gen = (Button)this.findViewById(R.id.gen);
    	check = (Button)this.findViewById(R.id.check);
    	word = (TextView)this.findViewById(R.id.word);
    	solution = (TextView)this.findViewById(R.id.solution);
    	input = (EditText)this.findViewById(R.id.input);
    	gen.setOnClickListener(this);
    	check.setOnClickListener(this);
    	
    }

	public void onClick(View v) {
		
		if(v.getId() == R.id.gen){
		
			theWord = words[r.nextInt(words.length)];
			String w = this.scramble(theWord);
			word.setText(w);
		
		}else{
			
			String inputWord = input.getText().toString();
			if(inputWord.equalsIgnoreCase(theWord)){
				
				solution.setText("You are correct :D");
				
			}else{
				
				solution.setText("You are incorrect :(");
				
			}
			
		}
		
	}
	
	public String scramble(String text){
		
		String scrambled = "";
		int l;
		int length = text.length();
		char c[] = new char[length];
		
		for (int i = 0; i < length; i++){
			
			c[i] = text.charAt(i);
			
		}
		
		while(scrambled.length()!=length){
			
			l = r.nextInt(length);
			if(c[l] != ' '){
				
				scrambled += c[l];
				c[l] = ' ';
			
			}
			
		}
		
		return scrambled;
		
	}

//	public boolean onKey(DialogInterface arg0, int arg1, KeyEvent arg2) {
//		
//		if(arg1 == Keyboard.KEYCODE_DONE || arg1 == KeyEvent.KEYCODE_ENTER ){
//			
//			InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
//			imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
//			return true;
//			
//		}else{
//		
//			return false;
//			
//		}
//		
//	}
    
}